#!/usr/bin/python3

import os, sys, dns.resolver

nargs = len(sys.argv)
zones = sys.argv
soatype = "SOA"
nstype = "NS"
atype = "A"
aaaatype = "AAAA"
period = "\{ . }"

query = dns.resolver.Resolver()
query.Timeout = 2.0

for zone in range (1,nargs):
        if zones[zone].endswith ("."):
                zones[zone] = zones[zone][:-1]
                print(zones[zone])
                exit
        for result in query.query(zones[zone]+"." , soatype):
                mname =  str(result.mname)
                mname = mname[:-1]
                hostmaster = str(result.rname)
                hostmaster = hostmaster[:-1]
                minimum = str(result.minimum)
                expire = str(result.expire)
                retry = str(result.retry)
                refresh = str(result.refresh)
                if zone == 1:
                        country = zones[zone].split(".")[-1].upper()
                        sysregistry = "REG-" + country;
                        sysregadd = "/usr/sbin/fred-admin --registrar_add --handle=" + sysregistry + " --country=" + country + " --reg_name=REG-CZ --organization=DEMO --street1=DEMO --city=DEMO --email=DEMO --url=DEMO --dic=12345 --no_vat --system"
                        print(sysregadd)
                        sysregacl = "/usr/sbin/fred-admin --registrar_acl_add --handle " + sysregistry + " --certificate 6A:AC:49:24:F8:32:1E:B7:A1:83:B5:D4:CB:74:29:98 --password passwd"
                        print(sysregacl)
                zoneadd = "/usr/sbin/fred-admin --zone_add --zone_fqdn " + zones[zone]  + " --hostmaster " + hostmaster + " --refresh " + refresh  + " --expiry " + expire + " --minimum " + minimum + " --update_retr " + retry + " --ns_fqdn " + mname
                print(zoneadd)
                pricelistc = "/usr/sbin/fred-admin --price_add --zone_fqdn " + zones[zone] +  " --operation CreateDomain --operation_price 00.00"
                print(pricelistc)
                pricelistr = "/usr/sbin/fred-admin --price_add --zone_fqdn " + zones[zone] +  " --operation RenewDomain --operation_price 00.00"
                print(pricelistr)

        for result in query.query( zones[zone]+"." , nstype ) :
                nsname = str(result)
                if nsname.endswith("."):
                        nsname = nsname[:-1]
                zonensadd = "/usr/sbin/fred-admin --zone_ns_add --zone_fqdn " + zones[zone]  + " --ns_fqdn " + nsname
                if  nsname.endswith(zones[zone]):
                        zonensadd = zonensadd +  " --addr "
                        try:
                                for nsresult in query.query(nsname,atype):
                                        ipaddr = str(nsresult)
                                        zonensadd = zonensadd + ipaddr + " "
                        except dns.resolver.NoAnswer:
                                pass
                        try:
                                for nsresult in query.query( nsname , aaaatype ) :
                                        ipaddr = str(nsresult)
                                        zonensadd = zonensadd + ipaddr + " "
                        except dns.resolver.NoAnswer:
                                pass
                if zonensadd.endswith(","):
                        zonensadd = zonensadd[:-1]
                print(zonensadd)


#!/bin/bash

# Check if script is running under root, otherwise fail
if [ "$EUID" -ne 0 ]
  then echo "Please run installation as root."
  exit
fi

START_DIR=$(pwd)

# Change directory to /tmp
cp -r files/ /tmp/
cd /tmp

# Install dependencies
apt update
apt install -y ca-certificates curl gnupg lsb-release python3-dnspython python3-pip uwsgi uwsgi-plugin-python3 whois nginx apache2

pip install aioitertools

# Docker installation
sudo mkdir -m 0755 -p /etc/apt/keyrings

# Add docker gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/docker.gpg

# Add docker repository
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install required docker packages
apt update
apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Create dummy network interface for docker
cat << EOT >> /etc/netplan/01-dock.yaml
network:
  version: 2
  renderer: networkd
  bridges:
    dock:
      dhcp4: false
      dhcp6: false
      accept-ra: false
      interfaces: [ ]
      addresses:
        - 192.168.1.1/32
EOT

netplan apply

# Add cznic keyring for fred packages
mkdir -p /usr/share/keyrings/
cp files/cznic-archive-keyring.gpg /usr/share/keyrings/cznic-archive-keyring.gpg

# Add source list for FRED
if [ ! -f /etc/apt/sources.list.d/fred.list ]; then
cat << EOT >> /etc/apt/sources.list.d/fred.list
deb [signed-by=/usr/share/keyrings/cznic-archive-keyring.gpg] http://archive.nic.cz/public $(lsb_release -sc) main
EOT
fi

# Run update with the new source list
apt update

# Postfix non-interactive installation
debconf-set-selections <<< "postfix postfix/mailname string $(hostname)"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
apt --assume-yes install postfix

# Install postgresql 13
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" |sudo tee  /etc/apt/sources.list.d/pgdg.list
apt update
apt -y install postgresql-13 postgresql-client-13

# Set psql timezone to UTC, set correct listen addresses for docker apps, and setup pg_hba correctly
sed -i~ -e "s/^#\?\s*timezone\s*=.*/timezone = 'UTC'/" /etc/postgresql/13/main/postgresql.conf
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = 'localhost,192.168.1.1'/" /etc/postgresql/13/main/postgresql.conf
echo "host    ferda           ferda           172.17.0.0/12           md5" | sudo tee -a /etc/postgresql/13/main/pg_hba.conf
systemctl restart postgresql

# Create preferences.d/ pin list to install correct fred packages
cp files/fred /etc/apt/preferences.d/fred

# Update repositories with the new pin list
apt update

# Installation of FRED
apt -y install fred-db fred-idl fred-typist python3-pydantic fred-api-logger fred-api-fileman fred-api-messenger fred-backend-logger fred-backend-logger-corba fred-backend-registry fred-backend-fileman fred-backend-messenger fred-backend-notify fred-backend-public-request fred-backend-zone fred-zone-generator fred-backend-dbreport fred-rifd fred-pifd fred-adifd fred-akmd fred-accifd python3-django-secretary fred-akm cdnskey-scanner libapache2-mod-corba libapache2-mod-whoisd libapache2-mod-eppd python3-fred-epplib

# Add eppic user and install fred-eppic under that user
adduser --quiet --disabled-password --shell /bin/bash --home /home/eppic --gecos "Eppic" eppic
su - eppic -c "pip install --user --index-url https://pypi.nic.cz/cznic/public fred-eppic"
sudo -u eppic bash -c 'echo "export PATH=\"$PATH:/home/eppic/.local/bin\"" >> ~/.bashrc'

# Uninstall new postgres installed with fred-db package
apt remove -y postgresql-17 postgresql-client-17
rm -r /etc/postgresql/17

# Initialize fred database
sed -i 's#/12/#/13/#g' /usr/sbin/fred-dbmanager
su - postgres -c "/usr/sbin/fred-dbmanager install"

# Copy DEMO configuration files into the /etc/fred folder
rm -rf /etc/fred/*
cp -r files/configs/* /etc/fred/

# Create system registrar, otherwise FRED services wont start
/usr/sbin/fred-admin --registrar_add --handle=REG-SYSTEM --reg_name=REG-SYSTEM --organization=SYSTEM --street1=SYSTEM --city=SYSTEM --email=SYSTEM --url=SYSTEM --country=CZ --dic=12345 --no_vat --system

# Create and initialize FRED messenger database
sudo -u postgres psql -c 'CREATE DATABASE messenger;'
sudo -u postgres psql -c "CREATE USER messenger WITH ENCRYPTED PASSWORD 'password';"
sudo -u postgres psql -c 'GRANT ALL PRIVILEGES ON DATABASE messenger TO messenger;'
MESSENGER_CONFIG=/etc/fred/messenger.conf alembic --config /etc/fred/messenger-alembic.ini upgrade head

# Enable secretary in UWSGI
cp /usr/share/doc/python3-django-secretary/examples/fred-secretary.ini /etc/uwsgi/apps-available/
ln -s /etc/uwsgi/apps-available/fred-secretary.ini /etc/uwsgi/apps-enabled/fred-secretary.ini
sed -i '/\/run\/uwsgi\/fred-secretary\/socket/c\socket = \/run\/uwsgi\/app\/fred-secretary\/socket' /etc/uwsgi/apps-available/fred-secretary.ini
cp /usr/share/doc/python3-django-secretary/examples/uwsgi_secretary.py /etc/uwsgi/

# Setup nginx and copy secretary configuration
cp /usr/share/doc/python3-django-secretary/examples/secretary-nginx.conf /etc/nginx/sites-available/secretary.conf
cp /usr/share/doc/python3-django-secretary/examples/fred-secretary.service /etc/systemd/system
ln -s /etc/nginx/sites-available/secretary.conf /etc/nginx/sites-enabled/secretary.conf
rm /etc/nginx/sites-enabled/default

# Create a folder for the UWSGI socket
mkdir -p /run/uwsgi/app/fred-secretary/
chown www-data:www-data /run/uwsgi/app/fred-secretary
# Create static files folder for secretary
mkdir -p /var/www/fred/
chown www-data:www-data /var/www/fred

# Create and initialize secretary database
sudo -u postgres psql -c 'CREATE DATABASE secretary;'
sudo -u postgres psql -c "CREATE USER secretary WITH ENCRYPTED PASSWORD 'passwd';"
sudo -u postgres psql -c 'GRANT ALL PRIVILEGES ON DATABASE secretary TO secretary;'
sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings django-admin migrate

# Collect secretary static files
sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings django-admin collectstatic

# Create secretary super user
sudo -u www-data DJANGO_SUPERUSER_USERNAME=admin DJANGO_SUPERUSER_PASSWORD=password DJANGO_SUPERUSER_EMAIL=admin@admin.com PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings django-admin createsuperuser --noinput

# Load secretary templates
apt install python3-docopt
cd /usr/share/doc/python3-django-secretary/examples/secretary-templates/
sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings python3 load_templates.py pdf-templates.yml
sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings python3 load_templates.py fred-migration.yml
sudo -u www-data PYTHONPATH=/etc/fred DJANGO_SETTINGS_MODULE=secretary_cfg.settings python3 load_templates.py fred-templates.yml

# Create log file for zone generator service
touch /var/log/fred-zone-services.log
chown fred /var/log/fred-zone-services.log

# Enable and start FRED services
systemctl daemon-reload
systemctl enable --now omniorb4-nameserver fred-accifd fred-adifd fred-akmd fred-backend-logger fred-logger-corba fred-backend-registry fred-pifd fred-rifd fred-secretary fred-messenger-server fred-fileman-server fred-zone-services fred-dbreport-services@messenger.service fred-dbreport-services@registry.service fred-dbreport-services@fredlog.service

# Enable apache2 modules required by EPP and WHOIS
a2enmod corba eppd ssl whoisd

# Enable eppd and whois sites in apache
a2ensite 02-fred-mod-eppd-apache 02-fred-mod-whoisd-apache

# Restart web servers to apply configurations
systemctl restart nginx
systemctl restart apache2

# Disable unused FRED services
systemctl disable fred-auctions-warehouse
systemctl mask fred-auction-warehouse.service
systemctl disable fred-dbreport-services.service
systemctl mask fred-dbreport-services.service

# Restart the services to be sure, that the config is correctly loaded
systemctl restart 'fred-*'
systemctl restart omniorb4-nameserver

# Create and initialize FERDA database
sudo -u postgres psql -c 'CREATE DATABASE ferda;'
sudo -u postgres psql -c "CREATE USER ferda WITH ENCRYPTED PASSWORD 'password';"
sudo -u postgres psql -c 'GRANT ALL PRIVILEGES ON DATABASE ferda TO ferda;'

# Deploy docker apps - Ferda, Webwhois, RDAP
apps=("ferda" "webwhois" "rdap")
WORKDIR=/etc/fred/docker-apps

# Initialize new docker swarm so we can have docker stack for each service
docker swarm init --advertise-addr 127.0.0.1

mkdir -p $WORKDIR
# We are doing the same process for every docker app - copy demo compose and .env file, pull images and deploy them using docker stack
for app in ${apps[@]}; do
        # Create directory structure for docker-apps and copy demo-deploy files
        mkdir -p $WORKDIR/$app
        cp -r /tmp/files/docker-apps/$app $WORKDIR/

        docker pull registry.nic.cz/fred/$app/$app-uwsgi
        docker pull registry.nic.cz/fred/$app/$app-nginx

        # Deploy docker stacks
        cd $WORKDIR/$app/
        docker stack deploy --compose-file docker-compose.yml $app
done

# Wait for FERDA container to start
echo -n "Waiting for FERDA container to start (this may take a while)"
while [[ $(docker ps | grep ferda_ferda_uwsgi | awk '{if ($4 > 15 || $4 == "About") print "OK"}') != "OK" ]]; do
    echo -n "."
    sleep 1
done

# Run Ferda database migrations and create superuser in FERDA

echo "Running database migrations for Ferda..."

docker exec  $(docker ps|grep ferda_ferda_uwsgi.1|cut -d ' ' -f1) django-admin migrate

echo "Creating Ferda superuser..."

docker exec -e DJANGO_SUPERUSER_USERNAME=admin -e DJANGO_SUPERUSER_EMAIL=admin@admin.demofred -e DJANGO_SUPERUSER_PASSWORD=password $(docker ps|grep ferda_ferda_uwsgi.1|cut -d ' ' -f1) django-admin createsuperuser --noinput

echo "Installation completed."

# Delete tmp files and go back where we started
rm -rf /tmp/files
cd $START_DIR
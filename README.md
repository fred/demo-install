<h1> FRED demo installion </h1>

This repository is used to maintain the installation scripts for FRED demo and its initialization script, individual scripts are described below.

<h2>install-fred.sh</h2>

The `install-fred.sh` script will install a clean FRED, configure it, and start the FRED-related docker applications. The script must be run as root.

Usage: 

```
/tmp#> wget -O fred-ubuntu-install.sh https://fred.nic.cz/public/media/1689167142/150/
/tmp#> . fred-ubuntu-install.sh
```

<h2>fred-config-zone.py</h2>

To test DEMO after installation it is necessary to create a test zone in FRED, for this purpose the script `fred-config-zone.sh` is used, which takes as input the parameter which zone will be initialized.

Usage:

```
/tmp#> wget -O fred-config-zone.py https://fred.nic.cz/public/media/1689167142/149/
/tmp#> python3 fred-config-zone.py cz > fred-config-cz.sh
/tmp#> . fred-config-cz.sh
```
